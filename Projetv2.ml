(*--------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 -----------              Projet de Programmation Fonctionnelle                                -----------
 -----------                        -     Léo BREGOIN                                          -----------
 -----------                        -   Theophile CARRASCO                                     -----------
 ---------------------------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------------------------*)


(* ----- Question 1 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Définir les types OCaml, pour les types et les expressions du langage mini-ML.

 ------------------------------------------------------------------------------------------------------- *)
type myType =
   Int
 |Char
 |Bool
 |Tfun of (myType * myType)
 |Tpair of (myType * myType)
;;

type myExpr =
  |X of string
  |C of myType
  |ExprPair of  (myExpr * myExpr)
  |ExprFun of string * myExpr
  |ExprLet of string * myExpr * myExpr
  |ExprApp of myExpr * myExpr
        

;;



(* ----- Question 2 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Définir une fonction "d’affichage" des expressions mini-ML qui prend une expression et retourne
   l’expression sous la forme d’une chaîne de caractères, écrite de manière habituelle.
      Définir quelques exemples d’expressions mini-ML et vérifier leur forme avec la fonction
   précédente

 ------------------------------------------------------------------------------------------------------- *)

(* Cette fonction permet de passer un myType en string afin de pouvoir l'afficher plus tard *)
let rec mytype_to_string (t : myType) : string =
  match t with
    Int -> " int "
   |Char -> " char "
   |Bool -> " bool "
   |Tfun (t1, t2) -> mytype_to_string(t1) ^ "->" ^ mytype_to_string(t2)
   |Tpair (t1,t2) -> mytype_to_string(t1) ^ "," ^ mytype_to_string(t2)
;;

(* print_expr permet de passer une expression en chaine de caractères, elle utilise mytype_to_string, et est la fonction auxilliaire à l'affichage final *)
let rec print_expr (e : myExpr) : string =
  match e with
    X(s) -> s
  |C(t) -> mytype_to_string(t)
  |ExprPair(e1,e2) ->" ( " ^ print_expr(e1) ^ ", " ^ print_expr(e2) ^ " ) "
  |ExprFun(x,e1) -> " fun " ^ x ^ print_expr(e1)
  |ExprLet(x,e1,e2) -> "let " ^ x ^ " : " ^ print_expr(e1) ^ print_expr(e2) ^ " in"
  |ExprApp(e1,e2) -> print_expr(e1) ^ print_expr(e2)
;;


(* print_expr_final permet d'afficher une expression *)
let print_expr_final (e : myExpr) : unit =
  print_string(print_expr(e))
;;

(* -----ZONE-DE-TEST------- *)

let t = Int(1);;

let ec = C(Tfun(Int, Bool));;
let ex = X("x");;

let e1 = X("1");;
let e2 = X("2");;

let pair = ExprPair(e1,e2);;
let local = ExprLet("x",ec, pair);;

print_expr_final(local);;






(* ----- Question 3 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Nous allons utiliser les listes d’associations OCaml pour représenter les environnements. 
      Définir l’environnements Ep comprenant les constantes et les opérations primitives du langage 
   mini-ML.

 ------------------------------------------------------------------------------------------------------- *)








(* ----- Question 4 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Définir une fonction de vérification de type, qui prend en argument un environnement et une
   expression mini-ML et retourne le type de l’expression si l’expression est bien typée et lève une
   erreur sinon. Cette fonction mettra en oeuvre les règles d’inférence monomorphe.

 ------------------------------------------------------------------------------------------------------- *)








(* ----- Question 5 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Utiliser la fonction de vérification de type de la question 4 pour calculer le type des 
   expressions mini-ML typé suivantes :
         fun x : char -> (let succ : int -> int = fun x : int -> + (x, 1) in (succ 1, x))
         fun x : char -> (let succ : int -> int = fun y : int -> + (y, 1) in (succ 1, x))
      Ont-elles le même type ? Sinon, modifier la fonction précédente pour prendre en compte les
   variables 
