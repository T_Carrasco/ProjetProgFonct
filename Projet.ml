(*--------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 -----------              Projet de Programmation Fonctionnelle                                -----------
 -----------                        -     Léo BREGOIN                                          -----------
 -----------                        -   Theophile CARRASCO                                     -----------
 ---------------------------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------------------------*)


(* ----- Question 1 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Définir les types OCaml, pour les types et les expressions du langage mini-ML.

 ------------------------------------------------------------------------------------------------------- *)
type myType =
   Int
 | Char
 | Bool
 | Tfun of (myType * myType)
 | Tpair of (myType * myType)
;;

type myConst =  
  | Un
  | Deux
  | Trois
  | Quatre
  | Cinq

  | A
  | B
  | C
  | D

  | True
  | False
  
  | Plus
  | Moins
  | Inf
  | Sup             
  | If_Then_Else
;;

type myExpr =
 | X   of string
 | Cst of myConst
 | ExprPair of (myExpr * myExpr)
 | ExprFun  of myType * myExpr
 | ExprLet  of myType * myExpr * myExpr
 | ExprApp  of myExpr * myExpr
;; 


;;
(* ----- Question 2 ------ Auteur : Léo BREGOIN --------------------------------------------------------

      Définir une fonction "d’affichage" des expressions mini-ML qui prend une expression et retourne
   l’expression sous la forme d’une chaîne de caractères, écrite de manière habituelle.
      Définir quelques exemples d’expressions mini-ML et vérifier leur forme avec la fonction
   précédente

 ------------------------------------------------------------------------------------------------------- *)

let rec mytype_to_string(t : myType) : string =
  match t with
    Int           -> " int "
   |Char          -> " char "
   |Bool          -> " bool "
   |Tfun (t1, t2) -> mytype_to_string(t1) ^ "->" ^ mytype_to_string(t2)
   |Tpair (t1,t2) -> mytype_to_string(t1) ^ "," ^ mytype_to_string(t2)
;;


let const_to_string(c : myConst) : string =
  match c with
  | Un      -> "1"
  | Deux    -> "2"
  | Trois   -> "3"
  | Quatre  -> "4"
  | Cinq    -> "5"
          
  | A       -> "a"
  | B       -> "b"
  | C       -> "c"
  | D       -> "d"
       
  | True    -> "true"
  | False   -> "false"
           
  | Plus    -> "+"
  | Moins   -> "-"
  | Inf     -> "<"
  | Sup     -> ">"               
  | If_Then_Else -> "if_then_else"
;;

(* print_expr permet de passer une expression en chaine de caractères, elle utilise mytype_to_string, et est la fonction auxilliaire à l'affichage final *)
let rec expr_to_string (e : myExpr) : string =
  match e with
    X(s)             -> s
  | Cst(c)             -> const_to_string(c)
  | ExprPair(e1,e2)  ->     " ( "      ^  expr_to_string(e1)   ^   ", "   ^   expr_to_string(e2)   ^   " ) "
  | ExprFun(x,e1)    ->     " fun "    ^   mytype_to_string(x)   ^   expr_to_string(e1)
  | ExprLet(x,e1,e2) ->     "let "     ^   mytype_to_string(x)   ^   " : "   ^   expr_to_string(e1)   ^   expr_to_string(e2)   ^   " in"
  | ExprApp(e1,e2)   -> expr_to_string(e1) ^ expr_to_string(e2)
;;


(* print_expr_final permet d'afficher une expression *)
let print_expr_final (e : myExpr) : unit =
  print_string(expr_to_string(e))
;;




(* -----ZONE-DE-TEST------- *)


;;
(* ----- Question 3 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Nous allons utiliser les listes d’associations OCaml pour représenter les environnements. 
      Définir l’environnements Ep comprenant les constantes et les opérations primitives du langage 
   mini-ML.

 ------------------------------------------------------------------------------------------------------- *)
type myEnvType = (myType * myExpr) list;;

let ep : myEnvType =
  [
    Int, Cst Un;
    Int, Cst Deux;
    Int, Cst Trois;
    Int, Cst Quatre; 
    Int, Cst Cinq; 
    
    Char, Cst A;
    Char, Cst B;
    Char, Cst C;
    Char, Cst D;

    Bool, Cst True;
    Bool, Cst False;
    
    Tfun(Tpair (Int,Int) , Int), Cst Plus;
    Tfun(Tpair (Int,Int) , Int), Cst Moins;
    Tfun(Tpair (Int,Int) , Bool), Cst Inf;
    Tfun(Tpair (Int,Int) , Bool), Cst Sup;
    Tfun(Tpair(Bool, Tpair (Int,Int)), Int), Cst If_Then_Else;
  ]
;;

let rec myEnvironnement_to_string(env : myEnvType) : string =
  match env with
  | [] -> ""
  | h::t -> expr_to_string(snd h) ^ " : " ^ mytype_to_string(fst h) ^ ";         " ^ myEnvironnement_to_string(t)
;;

myEnvironnement_to_string(ep);;

;;
(* ----- Question 4 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Définir une fonction de vérification de type, qui prend en argument un environnement et une
   expression mini-ML et retourne le type de l’expression si l’expression est bien typée et lève une
   erreur sinon. Cette fonction mettra en oeuvre les règles d’inférence monomorphe.

 ------------------------------------------------------------------------------------------------------- *)







;;
(* ----- Question 5 ------ Auteur : Theophile CARRASCO ---------------------------------------------------

      Utiliser la fonction de vérification de type de la question 4 pour calculer le type des 
   expressions mini-ML typé suivantes :
         fun x : char -> (let succ : int -> int = fun x : int -> + (x, 1) in (succ 1, x))
         fun x : char -> (let succ : int -> int = fun y : int -> + (y, 1) in (succ 1, x))
      Ont-elles le même type ? Sinon, modifier la fonction précédente pour prendre en compte les
   variables homonymes.

 ------------------------------------------------------------------------------------------------------- *)







;;
